import glob
import os

def main():

	script_list = glob.glob('*.py')
	pyc_list =  glob.glob('*.pyc')
	keep_list = [] 

	input = open('list_of_scripts.txt', 'r')
	for line in input:
		print "kept file: ", str(line).strip('\n')
		keep_list.append(str(line).strip('\n'))
	input.close()

	removal_list = []
	for python_script in script_list:
		if python_script not in keep_list:
			if python_script not in pyc_list:
				print python_script,"not found, removing"
				remove_command = "rm " + str(python_script)
				removal_list.append(python_script)
				os.system(remove_command)


	output = open('python_scripts_to_remove.txt','w')	

	for file_instance in removal_list:
		temp = '%s'%(file_instance)
		print>>output,temp

	output.close()
	 



if __name__ == "__main__":
	main()

