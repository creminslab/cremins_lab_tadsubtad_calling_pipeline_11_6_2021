from heatmap_removal import heatmap_removal
import numpy as np
import sys
import glob
import os
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import math


def find_bad_regions(settings, samples,tag_preprocess,tag_GPS, chromosomes,chrom):
 
	if 'uniform_size' not in  settings:
		settings['uniform_size'] = "False"
	if 'overlap' not in settings:
		overlap = int(math.ceil((float(settings['region_size'])*2)/3))
		settings['overlap'] = overlap

	print 'chromosomes: ', chromosomes
	total_badregions = []
	total_badregions_coords = []
	total_regions = []
	for sample in samples:
		total_finalvar = []
		regions, badregions, badregion_coords, finalvar = heatmap_removal(sample, chromosomes, tag_preprocess, tag_GPS, settings)
		print "regions after heatmap_removal: ",regions
		total_badregions = total_badregions + badregions
		total_regions = total_regions + regions
		total_badregions_coords = total_badregions_coords + badregion_coords
	masterlist_badregions = total_badregions
	masterlist_badregions = sorted(masterlist_badregions)
	masterlist_regions = total_regions
	masterlist_regions = sorted(masterlist_regions)
	total_badregions_coords = sorted(total_badregions_coords)


	unique_bad_regions = []
	for region in masterlist_badregions:
		if region not in unique_bad_regions:
			unique_bad_regions.append(region)

	unique_regions = []
	unique_region_coords = []
	for region in masterlist_regions:
		if region[0] not in unique_regions:
			unique_regions.append(region[0])
			unique_region_coords.append(region[1])
			

	print "unique_regions: ",unique_regions
	unique_good_regions = []
	unique_good_region_coords = []
	for i in range(len(unique_regions)):
		if unique_regions[i] not in unique_bad_regions:
			if eval(settings['uniform_size']):
				unique_good_regions.append(unique_regions[i])
				unique_good_region_coords.append(unique_region_coords[i])
			else:
				#still avoid using non-uniform region for good regions for max gamma determination
				if 'region_size' in settings:
					desired_region_size = (int(settings['region_size']) + 1)*int(settings['resolution'])
					stride = int(settings['region_size']) - int(settings['overlap'])
				else:
					desired_region_size = (int(settings['stride']) + int(settings['overlap']) + 1)*int(settings['resolution'])
					stride = int(settings['stride'])
				print "stride: ",stride
				print "desired_region_size: ",desired_region_size
				print "start: ",unique_region_coords[i]['start']
				print "end: ",unique_region_coords[i]['end']
				print "unique_region_coords[i]['end'] - unique_region_coords[i]['start']: ",unique_region_coords[i]['end'] - unique_region_coords[i]['start']
				if (unique_region_coords[i]['end'] - unique_region_coords[i]['start'] == desired_region_size):
					print "should be adding to good regions: "
					if stride < int(settings['overlap']) and unique_region_coords[i]['start'] != 0:
						unique_good_region_coords.append(unique_region_coords[i])
						unique_good_regions.append(unique_regions[i])
					elif stride >= int(settings['overlap']):
						unique_good_region_coords.append(unique_region_coords[i])
						unique_good_regions.append(unique_regions[i])

	print "unique_good_regions: ",unique_good_regions
	all_sample_tag = ''
	for sample in samples:
		all_sample_tag = all_sample_tag + sample + '_' 

	dir = 'output/GPS/bad_region_removal/'
	if not os.path.isdir(dir): os.makedirs(dir)
	fname = 'good_regions_' + all_sample_tag + tag_GPS + '_' + chrom + '.txt'
	outfile = open(dir + fname, 'w')
	unique_good_regions_list = []
	for region in unique_good_regions:
		output = region.keys()[0] + '\t' + region[region.keys()[0]]
		unique_good_regions_list.append((region.keys()[0],region[region.keys()[0]]))
		print >> outfile, output
	outfile.close()

	fname = 'bad_regions_' + all_sample_tag + tag_GPS + '_' + chrom + '.txt'
	outfile = open(dir + fname, 'w')
	#unique_bad_regions_list = []
	for region in unique_bad_regions:
		output = region.keys()[0] + '\t' + region[region.keys()[0]]
		#unique_bad_regions_list.append((region.keys()[0],region[region.keys()[0]))
		print >> outfile, output
	outfile.close()

	fname = 'bad_region_genomic_coords_' + all_sample_tag + tag_GPS + '_' + chrom + '.txt'
	outfile = open(dir + fname, 'w')
	total_badregions_coords_list =[]
	for coords in total_badregions_coords:
		output = coords['chrom'] + '\t' + str(coords['start']) + '\t' + str(coords['end'])
		total_badregions_coords_list.append((coords['chrom'],coords['start'],coords['end']))
		print >> outfile, output
	outfile.close()

	fname = 'good_region_genomic_coords_' + all_sample_tag + tag_GPS + '_' + chrom + '.txt'
	outfile = open(dir + fname, 'w')
	unique_good_region_coords_list = []
	for coords in unique_good_region_coords:
		output = coords['chrom'] + '\t' + str(coords['start']) + '\t' + str(coords['end'])
		unique_good_region_coords_list.append((coords['chrom'],coords['start'],coords['end']))
		print >> outfile, output
	outfile.close()
		


	return unique_bad_regions,unique_good_regions_list,total_badregions_coords_list,unique_good_region_coords_list

