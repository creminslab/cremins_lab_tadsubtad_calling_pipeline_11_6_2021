
**Introduction**
=============
3DNetMod is a package of python scripts that sensitively detects nested, hierarchical domains in HiC data.


**Installation**
==============
To run 3DNetMod, the source code must be downloaded from Bitbucket and its dependencies must also be installed. 
Below is a step by step guide to downloading the code from Bitbucket and installing all necessary dependencies using pip.

**Download source code from Bitbucket**
  ::
  
    git clone https://user@bitbucket.org/creminslab/cremins_lab_tadsubtad_calling_pipeline_11_6_2021.git
    
**Create a python virtual environment with the necessary dependencies** 
  ::
  
    conda install python=2.7.16
    conda install virtualenv
    virtualenv venv
    pip install numpy==1.16.1
    pip install scipy==1.2.1
    pip install matplotlib
    
    
**To run 3DNetMod, first activate the virtual environment you've created by running:**
  ::


    source venv/bin/activate
    



**Usage of 3DNetMod**
==================

**Step 1. Pre-process**
  ::
  
  
      python 3DNetMod_preprocess.py settings_chr#_chunk_size.txt
  
| The pre-processing step takes genome-wide HiC data and splits it into overlapping regions of specified chunk size to improve speed and sensitivity of domain detection (panel A below). 
|  
**Step 2. 3DNetMod-GPS-MMCP**
  ::
  
  
  
      python 3DNetMod_GPS_MMCP.py settings_chr#_chunk_size.txt
  
  
| For a given chunked size, the Gamma Plateau Sweep/Modularity Maximization and Consensus Partition (GPS-MMCP) steps identify domains across resolution scales by maximizing network modularity (panels B-G below).    
|  
**Step 3. 3DNetMod-HSVM**
  ::



      python 3DNetMod_HSVM_v2.py settings_chr#_chunk_size.txt (settings_chr#_chunk_size2.txt)
  
| The Hierarchical Spatial Variance Minimization (HSVM) step postprocessing merges overlapping domains, size thresholds domains, and readjusts nearly overlapping domain boundaries.  Optional merging of domains across multiple chunked size.  (Run prior steps1 and 2 for each chunked size).  Final output (chrom start end) placed in  output/HSVM/variance_thresholded_communities/merged/ as *adjust3.txt
|  
|
|  All parameters for the method are contained within settings_chr#_chunk_size.txt. Prior to running the commands above, place Hi-C .bed and .counts file in the input directory (see below).  
|
.. image:: overview.png
|
| 
**Input files**
============

Place Hi-C interaction data (one file for each Hi-C sample) and .bed (one for the set of Hi-C samples) files into the input/ subdirectory.

|

**Bed file form**
  ::
  

      chr1    0        10000     1
      chr1    10000    20000     2
      chr1    20000    30000     3
      chr1    30000    40000     4
      ....    .........    .........    .....
      chrX    178000000    178010000    240000   

  |
  |  1st col must be lower case chromosome (except X Y in chrX chrY). String value
  |  2nd col start genomic coord.  (Integer value)
  |  3rd col stop genomic coord.  (2nd col + resolution. Integer value)
  |  4th col bin number.  Integer value
  |  Tab delimited
  |

**matrix data file form**
  ::
    
    1     1    0.766649 
    1     2    10.98993
    1     3    56.00003
    1     4    3.222222
    2     2    0.988885
    2     3    0.400002
    3     3    5.344442
    3     4    10.22222   
    ....  ...  ........
  |
  | 1st col is bin1 of interaction pair.  Integer value
  | 2nd col is bin2 of interaction pair.  Integer value
  | 3rd col is counts interation value.   Float value
  | Tab delimited
  | Sorted ascending order of bin index for all column 1 

|   
**Settings file form**
  ::
 
    #PRE_PROCESSING#
    bed_file    HCT_116_noauxin_balanced_genomewide_10kb.bed
    sample    HCT116untreat10kbhg38merge
    counts_file   HCT_116_noauxin_balanced_genomewide_10kb_v2.counts
    overlap * 
    region_size * 
    logged  True
    processors    10  
    resolution    10000

    #GPS#
    badregionfilter True
    diagonal_density    0.65
    consecutive_diagonal_zero    50  
    scale   chr**
    plateau *  

    #HSVM#
    size_threshold    7   
    boundary_buffer 70000

|   Note: One settings file is required per chromosome condition.
|
**Output files**
=============
|
| Step 1 preprocess outputs: 
* chunked counts and bed file from region_size specified
* "chr#"*"sample_#"*finalpvalues.counts and "chr#"*"sample_#"*final.bed located in input/ directory.
* File names contain parameters used in the preprocess portion of settings file (see parameters section). Additionally each chromosome file is a "super region" (3 consecutive  regions of a chromosome) with form "chr#.supperregion#"
|
|
| Step 2 GPS_MMCP outputs: 
* List of regions per chromosome that were not considered based on deficiency of counts in region ("bad regions") and left over list of regions for determining calls ("good regions") are placed in **output/GPS/bad_region_removal**
* Max gamma found per chromosome and the selected gammas used per region based on max gamma placed in **output/GPS/gamma_dynamic_range**
* **GPS_MMCP pre-HSVM unfiltered calls used for step 3 in output/MMCP/unique_communities/results_files**
|
|
| Step 3 HSVM outputs:
* **Community call merging and adjustment based on Boundary_buffer parameter. See HSVM parameters below**
* **FINAL Boundary adjusted domains placed in output/HSVM/variance_thresholded_communities/merged/ as *adjust3.txt suffix per chromosome (chromosome start end)
|
|
**Parameters**
===========
|
| **All parameters are placed in settings.txt.**  Parameters are grouped under separate headings (preprocess, GPS,HSVM) that correspond to the stage of the method.  Filenames for each stage incorporate corresponding stage parameters. Parameters are summarized below. An asterisk after a parameter name (*) indicates that the parameter is optional and can be omitted.
|
   
|
| **##preprocess##**
+-----------------------------------+-----------------------------------------------------------+-----------------------------+
|    parameter                      |              Description                                  |      Recommended value      |
+===================================+===========================================================+=============================+
|  **bed_file**                     | Name of bedfile in input directory                        |          N/A                |
+-----------------------------------+-----------------------------------------------------------+-----------------------------+
|  **sample**                       | Label for the celltype condition.                         |          N/A                |                       
|                                   | Cannot have underscore in samplename                      |                             |
+-----------------------------------+-----------------------------------------------------------+-----------------------------+
|  **counts_file**                  | File name of the HiC interaction data for sample. This    |          N/A                |
|                                   | file should be placed in the input/ directory.            |                             |
+-----------------------------------+-----------------------------------------------------------+-----------------------------+
|  **region_size**                  | Size of chunked regions into which the genome should      | bins number corresponding   |
|                                   | be sliced.  Units are bins. Integer value.                | to 6 Mb recommended         |
|                                   |                                                           |                             | 
|                                   |                                                           | (additional chunking for    |
|                                   |                                                           | small domain sensitivity at |
|                                   |                                                           | high depth (> billion),     |
|                                   |                                                           | recommend  3 Mb)            |
|                                   |                                                           |                             |
+-----------------------------------+-----------------------------------------------------------+-----------------------------+
|  **overlap**\*                    | Optional parameter. Number of overlapping bins between    | default = 2/3 chunked       |
|                                   | adjacent regions.  Units are bins.  Integer value.        | region_size (bins)          |
|                                   | Can omit if default used.                                 |                             |
|                                   |                                                           |                             |
+-----------------------------------+-----------------------------------------------------------+-----------------------------+
|  **logged**                       | Option to log counts output. *True* or *False*. Use       | recommend *True* for        |
|                                   | *True* if input data are not logged and you wish to log   | logging counts              |  
|                                   | them. Use *False* if you do not wish to log the data.     |                             |
|                                   | (Counts < 1 set to zero prior to logging)                 |                             |
|                                   |                                                           |                             |
|                                   |                                                           |                             |
+-----------------------------------+-----------------------------------------------------------+-----------------------------+
|  **processors**                   | Number of computing cores utilized in                     | If running locally, set     |
|                                   | multiprocessing.Pool.  Depending on available resources,  | to the number of cores      |
|                                   | the maximum possible value should be number of chromosomes| available.                  | 
|                                   | Integer value.                                            |                             |
|                                   |                                                           |                             |
|                                   |                                                           |                             |
+-----------------------------------+-----------------------------------------------------------+-----------------------------+
|                                   |                                                           |                             |
|  **resolution**                   |  Basepair per bin                                         |         N/A                 |
|                                   |                                                           |                             |
+-----------------------------------+-----------------------------------------------------------+-----------------------------+

|
|
|
| **##GPS##**
+-----------------------------------+-----------------------------------------------------------+-----------------------------+
|    parameter                      |              Description                                  |      Recommended value      |
+===================================+===========================================================+=============================+
| **badregionfile**\*               | Optional parameter. Omit if not used. Name of a file      | default = None              |
|                                   | located in the main directory containing genomic          |                             |
|                                   | coordinates that should not be queried by 3DNetMod.       |                             |
|                                   |                                                           |                             |
|                                   |                                                           |                             |
|                                   |                                                           |                             | 
|                                   |                                                           |                             |
|                                   |                                                           |                             |
|                                   |                                                           |                             |
|                                   |                                                           |                             |
+-----------------------------------+-----------------------------------------------------------+-----------------------------+
| **badregionfilter**               | *True* or *False*. If True, regions with sparse counts    | *True* is recommended       |
|                                   | are removed from further analysis (see diagonal_density   |                             |
|                                   | and consecutive_diagonal_zero)                            |                             |
+-----------------------------------+-----------------------------------------------------------+-----------------------------+
| **diagonal_density**\*            | Optional parameter. Removes chunked regions that do not   | default = 0.65              |
|                                   | exceed PERCENT NONZERO in diagonal specified.  Default    |                             |
|                                   | value if not provided is 0.65. Can omit if default used.  |                             |
|                                   |                                                           |                             |
|                                   |                                                           |                             |
|                                   |                                                           |                             |
+-----------------------------------+-----------------------------------------------------------+-----------------------------+
| **consecutive_diagonal_zero**\*   | Optional parameter. Removes chunked regions where         | default = 50 bin            |
|                                   | consecutive ZEROs in diagonal exceeds number provided.    | (1 bin = resolution)        |
|                                   | bin number units.  Can omit if default used.              |                             |
|                                   |                                                           |                             |
|                                   |                                                           |                             |
|                                   |                                                           |                             |
|                                   |                                                           |                             |
+-----------------------------------+-----------------------------------------------------------+-----------------------------+
| **scale**                         | individual chromosome such as *chr1*,                     |      N/A                    |   
|                                   | *chr2*, *chrX*, etc. Initial input .bed and HiC           |                             |   
|                                   | interaction data MUST have the chromosome specified in    |                             |   
|                                   | the same format  (*chr*, *chr2*, *chrX*, etc).            |                             |   
+-----------------------------------+-----------------------------------------------------------+-----------------------------+
| **plateau**                       | Minimum number of consecutive gamma values with the same  | ~16 recommended for 6 Mb    |
|                                   | number of communities required for the gammas to be       | chunked region              |
|                                   | considered in a 'plateau' during GPS. A larger value will |                             |
|                                   | lead to fewer gamma values queried with MMCP and fewer    | ~8 recommended for 3 Mb     |
|                                   | resulting domains detected.                               | chunked region              |   
|                                   |                                                           |                             |   
+-----------------------------------+-----------------------------------------------------------+-----------------------------+


|
|
|
| **##HSVM##**
+-----------------------------------+-----------------------------------------------------------+-----------------------------+
|    parameter                      |              Description                                  |     Recommended Value       |
+===================================+===========================================================+=============================+
| **size_threshold**                | Lower size cutoff for domains (in units of bins).         | bin # corresponding to      |
|                                   | (<= size_threshold)                                       | ~70 kb recommended          |
+-----------------------------------+-----------------------------------------------------------+-----------------------------+
| **boundary_buffer**               | Tolerance in difference in domain boundary coordinates    | 60 - 80 kb recommended      |
|                                   | across replicates for two domains to be considered        |                             |
|                                   | the same domain and subject to merging.  Base pair units. |                             |
+-----------------------------------+-----------------------------------------------------------+-----------------------------+ 


**Getting Started**
=====================
| For first time users, we recommend that you do the following tutorial on test data supplied with the code to get started. 
| **First:** Follow the Installation instructions above for downloading the code and installing necessary dependencies.
| **Second:** Test data supplied in input/ for HCT (HCT_116_noauxin_balanced_genomewide_10kb_v2.counts.gz) and ES (H1hESC2.5_hg38_balanced_genomewide_10kb.counts.gz).  Unzip the datasets.
| **Third:** Run 3DNetMod using the parameters supplied in the settings_chr10_HCT_F_plat8_chunk1_merge.txt and settings_chr10_HCT_F_plat16_chunk1_merge.txtfile.  For extra sensitivity to small domains we ran two chunk sizes (3 Mb and 6 Mb) for step1 preprocess and step 2 GPS_MMCP phase and merged 6 and 3 Mb chunked output in HSVM postprocess phase.  We selected 16 plateau size for 6 Mb chunked regions and 8 plateau (HCT) or 6 plateau (ES) for 3 Mb chunked regions.
**Commands to run:**
  ::
  
      python 3DNetMod_preprocess.py settings_chr10_HCT_F_plat8_chunk1_merge.txt
      python 3DNetMod_GPS_MMCP.py settings_chr10_HCT_F_plat8_chunk1_merge.txt

      python 3DNetMod_preprocess.py settings_chr10_HCT_F_plat16_chunk2_merge.txt
      python 3DNetMod_GPS_MMCP.py settings_chr10_HCT_F_plat16_chunk2_merge.txt 

      python 3DNetMod_HSVM_v2.py settings_chr10_HCT_F_plat8_chunk1_merge.txt settings_chr10_HCT_F_plat16_chunk2_merge.txt

      (Note: can run single chunk postprocess for 3 Mb region with small domains only: python 3DNetMod_HSVM_v2.py settings_chr10_HCT_F_plat8_chunk1_merge.txt.  
       Can run single chunk postprocess for 6 Mb region with larger domains only: python 3DNetMod_HSVM_v2.py settings_chr10_HCT_F_plat16_chunk2_merge.txt)

| Final chromosome output in output/HSVM/variance_thresholded_communities/ as Merged_Communities_chr*_[sample]*adjust3.txt
| Note: if merging multiple chunks should only vary plateau size and region_size (chunk size).  Limited to at most three chunk sizes per chromosome.

**Tips for Fine Tuning Calls**
===========================
|
| **When experimenting with the region size and overlap between regions in the pre-processing step, smaller region sizes are more sensitive to smaller subTADs and less sensitive to larger TADs. Larger region sizes achieve greater sensitivity to larger TADs with less sensitivity to smaller TADs.**  However, from experience, the region size should not exceed 1200 bins due to the computational costs of re-wiring large regions and finding consensus partitions on large regions.
|

**FAQs for trouble-shooting**
============================
**I have too few/many domains.** There are 2 main parameters that can be adjusted to increase or decrease the number of domains that are identified:

1. **plateau**: this parameter defines how frequently gamma values are sampled when identifying domains. A smaller plateau value leads to more frequent gamma sampling, and thus more domains. The plateau size is defined in units of ‘gamma_step’. For example, a ‘plateau’ of 3 and a ‘gamma_step’ of 0.01 means that three consecutive gamma values in increments of 0.01 must have the same average number of communities/domains in order for the gamma value to be considered stable. If you have a low number of domains and are missing many sub-TADs, consider decreasing the plateau size. Conversely, if you want to decrease the number of domains identified, consider increasing the plateau size.

2. **size_threshold**: adjust minimum domain size prior to merging domains in HSVM step

