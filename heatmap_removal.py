import numpy as np
from compute_regional_spatial_variance import compute_regional_spatial_variance
from load_region_start_end_coord import load_region_start_end_coord
from counts_to_array import counts_to_array
from load_unqueriable import load_unqueriable
from check_intersect import check_intersect
from itertools import groupby
#from write_bad_subregions_chaos import write_bad_subregions


def get_consecutive_zeros(a):
	b = range(len(a))
	zero_intervals = []
	for group in groupby(iter(b), lambda x: a[x]):
		lis = list(group[1])
		if float(a[max(lis)]) == 0:  #only consecutive that happens to be zero
			zero_intervals.append(max(lis) - min(lis) + 1)
		else:
			zero_intervals.append(0) #treat as insignificant otherwise

	return max(zero_intervals)
	

def heatmap_removal(sample, chromosomes, tag_preprocess, tags_GPS, settings):

	badregion = []
	badregion_coords = []
	region_coords = []

	mean_variances = []   ### NOT sure if this is in the right place or not -- just putting here to get it running
	super_region_idx = {}

	if 'mean_var_threshold' not in settings:
		settings['mean_var_threshold'] = 'None' #default rate
	if 'diagonal_density' not in settings:
		settings['diagonal_density'] = 0.65
	if 'resolution' not in settings: #default resolution set to 40000
		settings['resolution'] = 10000
	if 'uniform_size' not in  settings:
		settings['uniform_size'] = "False"
	if 'consecutive_diagonal_zero' not in settings:
		#settings['consecutive_diagonal_zero'] = int(500000/int(settings['resolution']))
		settings['consecutive_diagonal_zero'] = 50


	for chr in chromosomes:
		
		counts_file = 'input/' + chr + '_' + sample  + '_' + tag_preprocess + 'finalpvalues.counts'
		bed_file = 'input/' + counts_file.split('/')[-1][:-14] + '.bed'
		region_idx, bin_size = load_region_start_end_coord(bed_file)
		super_region_idx[chr] = region_idx
		
		#open to allowing for shorter region selection for badregionfilter off, but for now want
		#uniform regions for gamma size selection
		#if eval(settings['badregionfilter']):
		if 'region_size' in settings:
			desired_region_size = (int(settings['region_size']) + 1)*bin_size
		else:
			desired_region_size = (int(settings['stride']) + int(settings['overlap']) + 1)*bin_size
		for region in region_idx:
			region_size = region_idx[region]['end'] - region_idx[region]['start']
			if region_size != desired_region_size:
				if eval(settings['uniform_size']):
					regiondict = {}
					regiondict[region] = chr
					badregion.append(regiondict)
					coords = super_region_idx[chr][region]
					badregion_coords.append(coords)
					print 'lost region for being incorrect size!'
			else:
				print 'region is correct size'
				


			# REMOVE SMALL REGIONS
		
		if 'badregionfile' in settings.keys():
			if settings['badregionfile'] != 'None':
				unqueriable = load_unqueriable(settings['badregionfile'])
				for region in region_idx:
					counter = 0
					for element in unqueriable:
						if check_intersect(element, region_idx[region]):
							counter +=1

					if counter > 0:
						regiondict = {}
						regiondict[region] = chr
						badregion.append(regiondict)
						coords = super_region_idx[chr][region] 
						badregion_coords.append(coords)

	
	window = 20  #chaos filter window
	slice_start = 2*(40000/int(settings['resolution'])) #adjust start depending on resolution
	slice_stop = slice_start + 8 #10*(40000/int(settings['resolution'])) #adjust stop depending on resolution, previous endpt (old_output)
	
	metrics = {}
	metrics['maxs']={}
	metrics['variances'] = {}

	for i in range(slice_start,slice_stop+1):
		metrics['maxs'][i] = []
		metrics['variances'][i] = []
	metrics['maxs']['slice'] = []
	metrics['variances']['slice'] = []


	regions = []
	for chr in chromosomes:

		counts_file = 'input/' + chr + '_' + sample  + '_' + tag_preprocess + 'finalpvalues.counts'
		bed_file = 'input/' + counts_file.split('/')[-1][:-14] + '.bed'

		counts_as_arrays,bin_location = counts_to_array(counts_file, bed_file)		
		for region in counts_as_arrays:
			region_dict = {}
			region_dict[region] = chr
			A = counts_as_arrays[region]		
			if settings['mean_var_threshold'] != 'None':
				mean_var = compute_regional_spatial_variance(counts_as_arrays[region], 3)
			else:
				mean_var = 'None'
			mean_variances.append(mean_var)
			diag = A.diagonal()
			iter = 0
			slice1 = diag[:-1]
			slice2 = diag[:-1]
			diag = slice2
			greater_0 = np.count_nonzero(diag)
			pct_greater_0 = float(greater_0)/float(len(diag))
			consecutive_zeros = get_consecutive_zeros(diag)
			if eval(settings['badregionfilter']):
				if settings['mean_var_threshold'] != 'None':
					if mean_var > float(settings['mean_var_threshold']):
						coords = super_region_idx[chr][region]
						badregion_coords.append(coords)
						badregion.append(region_dict)
				if pct_greater_0 < float(settings['diagonal_density']):
					coords = super_region_idx[chr][region]
					badregion_coords.append(coords)
					badregion.append(region_dict)
				if consecutive_zeros > int(settings['consecutive_diagonal_zero']):
					coords = super_region_idx[chr][region]
					badregion_coords.append(coords)
					badregion.append(region_dict)
			coords = super_region_idx[chr][region]
			regions.append((region_dict, coords))

			if region not in badregion:
				for i in range(slice_start,slice_stop+1):
					slice = A.diagonal(offset=i)
					slice = slice[slice>0] # don't want to consider zeros when computing mean or variance
					if len(slice) > 0:
						metrics['maxs'][i].append(max(slice))
						metrics['variances'][i].append(np.var(slice, ddof=1))


	return regions, badregion, badregion_coords, mean_variances
				






	 
