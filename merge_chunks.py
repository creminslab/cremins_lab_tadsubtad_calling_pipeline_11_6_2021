from read_settings import read_settings
from load_community_dict import load_community_dict
from load_region_start_end_coord import load_region_start_end_coord
from create_tags import create_tags
from hierarchical_spatial_variance_minimization import HSVM
import sys 
import glob
import os
from remove_chaos_communities import remove_chaos_communities
from write_consistent_communities import write_consistent_communities
import numpy as np

def load_intervals_merging_chunks(results_path):
	input = open(results_path, 'r')
	communities = {}  

	for line in input:
		if line.startswith('#'):
			elements = line.strip().split('\t')
			test = len(elements)
		else:
			chromosome = str(line.strip().split('\t')[0])
			start1 = int(line.strip().split('\t')[1])
			start2 = int(line.strip().split('\t')[2])
			end1 = int(line.strip().split('\t')[3])
			end2 = int(line.strip().split('\t')[4])
			left_var = float(line.strip().split('\t')[5])
			right_var = float(line.strip().split('\t')[6])
			if chromosome not in communities:
				communities[chromosome] = []
			communities[chromosome].append([chromosome,start1,start2,end1,end2,left_var,right_var])
	input.close()

	return communities


def merge_chunks(setting_collections,HSVM_per_chunk_collection):

	""" Merge domains into one set.  At most 3 chunks can be used.

	"""

	settings1 = read_settings(setting_collections[0])
	tags1 = create_tags(setting_collections[0])
	tags_HSVM1 = tags1[3]

	if 'sample_1' in settings1:
		sample1 = settings1['sample_1']
	else:
		sample1 = settings1['sample']

	file1 = 'Merged_' + 'Communities_' + settings1['scale'] + sample1 + '_' + tags_HSVM1 + '.txt'

	if len(setting_collections) == 2:
		settings2 = read_settings(setting_collections[1])
		tags2 = create_tags(setting_collections[1])
		tags_HSVM2 = tags2[3]

		if 'sample_1' in settings2:
			sample2 = settings2['sample_1']
		else:
			sample2 = settings2['sample']
		
		file2 = 'Merged_' + 'Communities_' + settings2['scale'] + sample2 + '_' + tags_HSVM2 + '.txt'
		
	if len(setting_collections) > 2:
		settings3 = read_settings(setting_collections[2])
		tags3 = create_tags(setting_collections[2])
		tags_HSVM3 = tags3[3]

		if 'sample_1' in settings3:
			sample3 = settings3['sample_1']
		else:
			sample3 = settings3['sample']

		file3 = 'Merged_' + 'Communities_' +  settings3['scale'] + sample3 + '_' + tags_HSVM3 + '.txt'

	dir = 'output/HSVM/variance_thresholded_communities/merged/'

   
	community_list1 = HSVM_per_chunk_collection[0] #load_intervals_merging_chunks(file1)
	if len(setting_collections) == 2:
		community_list2 = HSVM_per_chunk_collection[1] #load_intervals_merging_chunks(file2) 
	if len(setting_collections) > 2:
		community_list3 = HSVM_per_chunk_collection[2] #load_intervals_merging_chunks(file3)



	if 'overlap' in settings1:
		original_parameters = settings1['overlap'] + '_' + settings1['region_size']
	else:
		original_parameters = '_' + settings1['region_size'] + '_'

	if len(setting_collections) > 2:
		if (settings1['size_threshold'] != settings2['size_threshold']) or (settings1['size_threshold'] != settings3['size_threshold']):
			raise ValueError('different minimum size filters for different settings!')
		if (settings1['boundary_buffer'] != settings2['boundary_buffer']) or (settings1['boundary_buffer'] != settings3['boundary_buffer']):
			raise ValueError('different merging buffer for different settings!')
		if 'overlap' in settings1:
			total_parameters = settings1['overlap'] + '_' + settings1['region_size'] + '_' + settings2['overlap'] + '_' + settings2['region_size'] + \
			'_' + settings3['overlap'] + '_' + settings3['region_size']
		else:
			total_parameters = '_' + settings1['region_size'] + '_' + settings2['region_size'] + '_' + settings3['region_size'] + '_'
		output_file = file1.replace(original_parameters,total_parameters)

	elif len(setting_collections) == 2:
		if (settings1['size_threshold'] != settings2['size_threshold']):
			raise ValueError('different minimum size filters for different settings!')
		if (settings1['boundary_buffer'] != settings2['boundary_buffer']):
			 raise ValueError('different merging buffer for different settings!')

		if 'overlap' in settings1:
			total_parameters = settings1['overlap'] + '_' + settings1['region_size'] + '_' + settings2['overlap'] + '_' + settings2['region_size']
		else:
			total_parameters = '_' + settings1['region_size'] + '_' + settings2['region_size'] + '_'
		output_file = file1.replace(original_parameters,total_parameters)
	else:
		output_file = file1

	#original_parameters = settings1['scale'] + '_' + settings1['plateau'] + '_'
	if len(setting_collections) > 2:
		total_parameters = settings1['scale'] + '_' + settings1['plateau'] + '_' + settings2['plateau'] + '_' + settings3['plateau'] + '_'
	elif len(setting_collections) == 2:
		total_parameters = settings1['scale'] + '_' + settings1['plateau'] + '_' + settings2['plateau'] + '_'
	else:
		total_parameters = settings1['scale'] + '_' + settings1['plateau']

	total_calls = {}


	#sort to prevent order of files affecting result
	if len(setting_collections) > 2:
		chromosomes = list(set(community_list1.keys() + community_list2.keys() + community_list3.keys()))
	elif len(setting_collections) == 2:
		chromosomes = list(set(community_list1.keys() + community_list2.keys()))
	else:
		chromosomes = list(set(community_list1.keys()))

	for chrom in chromosomes:
		if len(setting_collections) > 2:
			total_calls[chrom] = community_list1[chrom] + community_list2[chrom] + community_list3[chrom] 
		elif len(setting_collections) == 2:
			total_calls[chrom] = community_list1[chrom] + community_list2[chrom] 
		else:
			total_calls[chrom] = community_list1[chrom]
		total_calls[chrom].sort()  

	output_file = output_file.replace(original_parameters,total_parameters)


	total_calls_return = []
	for chrom in total_calls:
		for i in range(len(total_calls[chrom])):
			start1 = total_calls[chrom][i][1]
			start2 = total_calls[chrom][i][2]
			end1 = total_calls[chrom][i][3]
			end2 = total_calls[chrom][i][4]
			left_var = total_calls[chrom][i][5]
			right_var = total_calls[chrom][i][6]
			total_calls_return.append({"chrom":chrom,'start':start1,'end':end2,'left_var':left_var,'right_var':right_var})


	return total_calls_return



if __name__ == "__main__":
	merge_chunks()


			


	
