from read_settings import read_settings
from load_intervals import load_intervals
from create_tags import create_tags
from operator import itemgetter
import argparse
import numpy as np
import glob


""" 
    WARNING: script works on a per chromosome file.  Have not adjusted script to handle genomewide
    calls.
"""

def  refine_final_calls(community_list,buffer_genome_2):
    """
        Group domains that fall within +- buffer_genome_2 (ideally use same criterion from HSVM
    """


    repository = []
    sorted_domain = []
    refine_communities = []

    #for j in range(len(community_list)): #rearrange to make sure consideration is based on earliest coord to latest
    #    sorted_domain.append((community_list[j][1],j))
    sorted_domain = sorted(community_list, key=itemgetter('start')) 

    #sorted_domain = sorted(sorted_domain,key=itemgetter(0))

    for j in range(len(sorted_domain)):
        chrom = sorted_domain[j]["chrom"]
        start = int(sorted_domain[j]["start"])
        end = int(sorted_domain[j]["end"])
        left_var = sorted_domain[j]["left_var"]
        right_var = sorted_domain[j]["right_var"]
        not_found = True #add to reposiory if not found

        if start == end:
            print "found community with non-unique start/end"
            print "start: ", community_list[j]['start']
            print "end: ",community_list[j]['end']  
            continue #skip over domains that are single boundary
        if len(repository) == 0:
            repository.append([chrom, start - buffer_genome_2, start + buffer_genome_2,end - buffer_genome_2,end + buffer_genome_2, left_var,right_var])
            continue
        for k in range(len(repository)):
            if repository[k][1] <= start <= repository[k][2]:
                if repository[k][3] <= end <= repository[k][4]:
                    not_found = False

                    consideration_start_1 = start - buffer_genome_2
                    consideration_start_2 = start + buffer_genome_2
                    consideration_end_1 = end - buffer_genome_2
                    consideration_end_2 = end + buffer_genome_2
                
                    if start == repository[k][1]:
                        repository[k][1] = consideration_start_1
                    elif start == repository[k][2]:
                        repository[k][2] = consideration_start_2
                    if end == repository[k][3]:
                        repository[k][3] = consideration_end_1
                    elif end == repository[k][4]:
                        repository[k][4] = consideration_end_2


                    repository[k][5] = np.max([left_var,repository[k][5]])
                    repository[k][6] = np.max([left_var,repository[k][6]])
                    break

        if not_found:
            repository.append([chrom, start - buffer_genome_2, start + buffer_genome_2,end - buffer_genome_2,end + buffer_genome_2, left_var,right_var])

    for j in range(len(repository)):
        community = {}
        community['chrom'] = repository[j][0]
        community['start'] = repository[j][1] + buffer_genome_2 #get back actual min
        community['end'] = repository[j][4] - buffer_genome_2 # get back actual max
        community['left_var'] = repository[j][5]
        community['right_var'] = repository[j][6]

        refine_communities.append(community)

    return refine_communities


def test_uniqueness(community,unique_boundaries,collection,buffer_genome):

    #main idea is that if matchup is found between community start or end boundary (within thresholds), corresponding community boundary will be reassigned
    #to original unique boundary (principal) in list.  This value serves as a key to list of boundaries that were found to be within threshold of principal
    #from all boundaries.  In later function, the actual final boundary for community will use the key to access list and take mean of boundary list, to 
    #which the actual boundary will be assigned 


    notkey_start = True
    notkey_end = True

    community_size = np.abs(community['start'] - community['end'])

    for i in range(len(unique_boundaries)): 
   

        unique_size = np.abs(unique_boundaries[i]['end'] - unique_boundaries[i]['start'])

        #test if call lines up perfectly with call already present.  Tests if gaps between boundaries are <= 10% of call size present or both boundaries are within 6 bins of call size present. 
        #Should not be used since classify_domains_compro_refine should handle this        
        if np.abs(community['start'] - unique_boundaries[i]['start']) <= buffer_genome and np.abs(community['end'] - unique_boundaries[i]['end']) <= buffer_genome:
            return [],unique_boundaries,collection,notkey_start,notkey_end
        elif float(np.abs(community['start'] - unique_boundaries[i]['start']) + np.abs(community['end'] - unique_boundaries[i]['end']))/float(unique_size) <= 0.1:
            return [],unique_boundaries,collection,notkey_start,notkey_end

        #after testing redundancy only adjust right or left boundary of call (if correspond to previous call)
        #compare start to end
        if np.abs(community['start'] - unique_boundaries[i]['end']) <= buffer_genome:
            #if start is <= 6 bins from boundary in list, readjust start
            #store list of boundaries that correspond to principle boundary
            collection[unique_boundaries[i]['end']].append(community['start'])
            community['start'] = unique_boundaries[i]['end']
            notkey_start = False
            break
        elif (float(np.abs(community['start'] - unique_boundaries[i]['end']))/float(community_size) <= 0.075) and (float(np.abs(community['start'] - unique_boundaries[i]['end']))/float(unique_size) <= 0.075):
            #if boundary gap is <= 10% of domain tested, readjust start
            #store list of boundaries that correspond to principle boundary
            collection[unique_boundaries[i]['end']].append(community['start'])
            community['start'] = unique_boundaries[i]['end']
            notkey_start = False
            break

        #compare start to start
        if np.abs(community['start'] - unique_boundaries[i]['start']) <= buffer_genome:
            #if start is <= 6 bins from boundary in list, readjust start
            #store list of boundaries that correspond to principle boundary
            collection[unique_boundaries[i]['start']].append(community['start'])
            community['start'] = unique_boundaries[i]['start']
            notkey_start = False
            break
        elif (float(np.abs(community['start'] - unique_boundaries[i]['start']))/float(community_size) <= 0.075) and (float(np.abs(community['start'] - unique_boundaries[i]['start']))/float(unique_size) <= 0.075): 
            #if boundary gap is <= 10% of domain tested, readjust start
            #store list of boundaries that correspond to principle boundary
            collection[unique_boundaries[i]['start']].append(community['start'])
            community['start'] = unique_boundaries[i]['start']
            notkey_start = False
            break


    for i in range(len(unique_boundaries)):

         unique_size = np.abs(unique_boundaries[i]['end'] - unique_boundaries[i]['start'])

         if np.abs(community['start'] - unique_boundaries[i]['start']) <= buffer_genome and np.abs(community['end'] - unique_boundaries[i]['end']) <= buffer_genome:
             return [],unique_boundaries,collection,notkey_start,notkey_end
         elif float(np.abs(community['start'] - unique_boundaries[i]['start']) + np.abs(community['end'] - unique_boundaries[i]['end']))/float(unique_size)  <= 0.1:
             return [],unique_boundaries,collection,notkey_start,notkey_end

         #after testing redundancy only adjust right or left boundary of call (if correspond to previous call)
         #compare end to end
         if np.abs(community['end'] - unique_boundaries[i]['end']) <= buffer_genome:
             #if start is <= 6 bins from boundary in list, readjust start
             #store list of boundaries that correspond to principle boundary
             collection[unique_boundaries[i]['end']].append(community['end'])
             community['end'] = unique_boundaries[i]['end']
             notkey_end = False
             break
         elif (float(np.abs(community['end'] - unique_boundaries[i]['end']))/float(community_size) <= 0.075) and (float(np.abs(community['end'] - unique_boundaries[i]['end']))/float(unique_size) <= 0.075):
             #if boundary gap is <= 10% of domain tested, readjust start
             #store list of boundaries that correspond to principle boundary
             collection[unique_boundaries[i]['end']].append(community['end'])
             community['end'] = unique_boundaries[i]['end']
             notkey_end = False
             break

         #compare end to start
         if np.abs(community['end'] - unique_boundaries[i]['start']) <= buffer_genome:
             #if start is <= 6 bins from boundary in list, readjust start
             #store list of boundaries that correspond to principle boundary
             collection[unique_boundaries[i]['start']].append(community['end'])
             community['end'] = unique_boundaries[i]['start']
             notkey_end = False
             break
         elif (float(np.abs(community['end'] - unique_boundaries[i]['start']))/float(community_size) <= 0.075) and (float(np.abs(community['end'] - unique_boundaries[i]['start']))/float(unique_size) <= 0.075) :
             #if boundary gap is <= 10% of domain tested, readjust start
             #store list of boundaries that correspond to principle boundary
             
             collection[unique_boundaries[i]['start']].append(community['end'])
             community['end'] = unique_boundaries[i]['start']
             notkey_end = False
             break
    if community['end']  == community['start']:
        print "adjusted community to same boundary at start and end!"
        print "removing community from consideration"
        print "adjusted to unique start: ",community['start']
        print "adjusted to unique end: ", community['end']
        print "collection size at start before: ",len(collection[community['start']])      
        print "collection size at end before: ",len(collection[community['end']]) 
        
        del collection[community['end']][-1]
        del collection[community['start']][-1]
        
        print "purged original start and end from collection"
        print "collection size at start after: ",len(collection[community['start']]) 
        print "collection size at end after: ",len(collection[community['end']]) 
        community = []

    return (community,unique_boundaries,collection,notkey_start,notkey_end)

def unique_difference(collection,args):

    difference_total = []

    #collect difference from mean in collection
    for principal in collection:
        
        mean_boundary = int(np.floor(np.mean(collection[principal])/args.resolution))*args.resolution

        for i in range(len(collection[principal])):
            difference = np.abs(mean_boundary - collection[principal][i])
            difference_total.append((mean_boundary,collection[principal][i],difference))

    return difference_total


def adjust_unique(unique_boundaries,collection,args):

    #go back to collection and find start and end key that correspond to unique start and end and adjust to average of list 
    mark_removal = []
    final_unique = []
    for i in range(len(unique_boundaries)):

        start = int(np.floor(np.mean(collection[unique_boundaries[i]['start']])/args.resolution))*args.resolution
        end = int(np.floor(np.mean(collection[unique_boundaries[i]['end']])/args.resolution))*args.resolution

        unique_boundaries[i]['start'] = start #np.mean(collection[unique_boundaries[i]['start']])
        unique_boundaries[i]['end'] = end #np.mean(collection[unique_boundaries[i]['end']])
        if start == end:
            mark_removal.append(i)

    for i in range(len(unique_boundaries)):
        if i not in mark_removal:
            final_unique.append(unique_boundaries[i])  

    #return unique_boundaries
    return final_unique

def boundary_assessment(community_list,buffer_genome,args):

    comparison = 0
    collection = {}
    unique_boundaries = []

    for j in range(len(community_list)):
        if community_list[j]['start'] == community_list[j]['end']:
            print "found community with non-unique start/end"
            print "start: ", community_list[j]['start']
            print "end: ",community_list[j]['end']  
            continue
        if len(collection) == 0:
            collection[community_list[j]['start']] = []
            collection[community_list[j]['end']] = []
            collection[community_list[j]['start']].append(community_list[j]['start'])
            collection[community_list[j]['end']].append(community_list[j]['end'])
            unique_boundaries.append(community_list[j])
        else:
            #compare boundary to list of current unique boundaries
            output = test_uniqueness(community_list[j],unique_boundaries,collection,buffer_genome)

            adjusted_community = output[0]
            unique_boundaries = output[1]
            collection = output[2]
            notkey_start = output[3]
            notkey_end = output[4]

            if len(adjusted_community) > 0:
                unique_boundaries.append(adjusted_community)
                #print "adjusted_community",adjusted_community
                if notkey_start:
                    collection[adjusted_community['start']] = []
                    collection[adjusted_community['start']].append(adjusted_community['start'])
                if notkey_end:
                    collection[adjusted_community['end']] = []
                    collection[adjusted_community['end']].append(adjusted_community['end'])

    statistics = unique_difference(collection,args)
    unique_boundaries = adjust_unique(unique_boundaries,collection,args)

    return unique_boundaries,statistics                        
    
def final_calls(unique_boundaries,file,args):

    output = open(file[:-4] + '_adjust3.txt', 'w')

    unique_filtered = {}
    for j in range(len(unique_boundaries)):
        chrom = unique_boundaries[j]['chrom']
        start = unique_boundaries[j]['start']
        end = unique_boundaries[j]['end']
        left_var = unique_boundaries[j]['left_var']
        right_var = unique_boundaries[j]['right_var']
        total_var = left_var + right_var
        if chrom not in unique_filtered:
            unique_filtered[chrom] = {}
            unique_filtered[chrom]['domain'] = []
            unique_filtered[chrom]['variance'] = []  
        if [chrom,start,end] not in unique_filtered[chrom]['domain']:
            unique_filtered[chrom]['domain'].append([chrom,start,end])
            unique_filtered[chrom]['variance'].append([left_var,right_var])
        else:
            index = unique_filtered[chrom]['domain'].index([chrom,start,end])
            if total_var > unique_filtered[chrom]['variance'][index][0] + unique_filtered[chrom]['variance'][index][1]:
                unique_filtered[chrom]['variance'][index][0] = left_var
                unique_filtered[chrom]['variance'][index][0] = right_var  


    for chrom in unique_filtered:
        for j in range(len(unique_filtered[chrom]['domain'])):
            chrom = unique_filtered[chrom]['domain'][j][0]
            start = unique_filtered[chrom]['domain'][j][1]
            end = unique_filtered[chrom]['domain'][j][2]
            left_var = unique_filtered[chrom]['variance'][j][0]
            right_var = unique_filtered[chrom]['variance'][j][1]

            temp =  '%s\t%d\t%d\t%f\t%f'%(chrom,start,end,left_var,right_var)
            print>>output,temp

    output.close()


    
def final_boundaries(unique_boundaries,file,args):


    boundaries = []
    storage = []

    output = open(file[:-4] + '_adjust3_b.txt', 'w')

    for j in range(len(unique_boundaries)):
        chrom = unique_boundaries[j]['chrom']
        start = unique_boundaries[j]['start']
        end = unique_boundaries[j]['end']
        left_var = unique_boundaries[j]['left_var']
        right_var = unique_boundaries[j]['right_var']

        if start not in boundaries:
            boundaries.append(start)
            storage.append((chrom,start))
        if end not in boundaries:
             boundaries.append(end)
             storage.append((chrom,end))

    for j in range(len(storage)):
        temp =  '%s\t%d'%(storage[j][0],storage[j][1])
        print>>output,temp

    output.close()

def print_statistics(statistics,file,args):

    output = open(file[:-4] + '_adjust3_difference.txt', 'w')

    temp = '%s\t%s\t%s\t%s'%('#chr','mean_adjusted','original_boundary','difference')
    print>>output,temp      

    for j in range(len(statistics)):
        temp =  '%s\t%d\t%d\t%d'%(args.chr,statistics[j][0],statistics[j][1],statistics[j][2])
        print>>output,temp       
 
    output.close()


def main():

    """
         final update and removal of nonunique domains '*adjust3.txt' domains and '*adjust3_b.txt' boundaries

    """

    parser = argparse.ArgumentParser()
    parser.add_argument('chr', type=str, help = "chromosome")
    parser.add_argument('resolution',type=int, help = "resolution")
    parser.add_argument('replicate', type=str, help="replicate")
    parser.add_argument('buffer', type=int, help="buffer around boundaries")
    parser.add_argument('settings_file1',type=str,help="settings_file")
    parser.add_argument('settings_file2',type=str,help="settings_file, None will not search for it")
    parser.add_argument('settings_file3',type=str,help="settings_file, None will not search for it.  If not None assumes file2 not None.")

    args = parser.parse_args()

    settings1 = read_settings(args.settings_file1)

    tags1 = create_tags(args.settings_file1)
    tag_HSVM1 = tags1[3]
    
    dir = 'output/HSVM/variance_thresholded_communities/merged/'

    file1_name = 'Merged_' + 'Communities_*' + settings1['sample_1'] + '_' + tag_HSVM1 + '.txt'
    file1 = glob.glob(dir + file1_name)[0]

    print file1


    
    if args.settings_file3 != 'None':
        settings3 = read_settings(args.settings_file3)
        tags3 = create_tags(args.settings_file3)
        tag_HSVM3 = tags3[3]   

        settings2 = read_settings(args.settings_file2)
        tags2 = create_tags(args.settings_file2)
        tag_HSVM2 = tags3[3]   

        if (settings1['size_threshold'] != settings2['size_threshold']) or (settings1['size_threshold'] != settings3['size_threshold']):
            raise ValueError('different minimum size filters for different settings!')
        if (settings1['boundary_buffer'] != settings2['boundary_buffer']) or (settings1['boundary_buffer'] != settings3['boundary_buffer']):
            raise ValueError('different merging buffer for different settings!')

        original_parameters = '_' + settings1['overlap'] + '_' + settings1['region_size'] + '_'
        total_parameters = '_' + settings1['overlap'] + '_' + settings1['region_size'] + '_' + settings2['overlap'] + '_' + settings2['region_size'] + '_'\
            + settings3['overlap'] + '_' + settings3['region_size'] + '_'
    
        original_parameters2 = '_' + settings1['scale'] + '_' + settings1['plateau'] + '_'
        total_parameters2 = '_' + settings1['scale'] + '_' + settings1['plateau'] + '_' + settings2['plateau'] + '_' + settings3['plateau'] + '_'
           

        total_file = file1.replace(original_parameters,total_parameters)
        total_file = total_file.replace(original_parameters2,total_parameters2)

        print total_file
    elif args.settings_file2 != 'None':
        settings2 = read_settings(args.settings_file2)
        tags2 = create_tags(args.settings_file2)
        tag_HSVM2 = tags2[3]   

        if (settings1['size_threshold'] != settings2['size_threshold']):
            raise ValueError('different minimum size filters for different settings!')
        if (settings1['boundary_buffer'] != settings2['boundary_buffer']):
             raise ValueError('different merging buffer for different settings!')

        original_parameters = '_' + settings1['overlap'] + '_' + settings1['region_size'] + '_' 
        total_parameters = '_' + settings1['overlap'] + '_' + settings1['region_size'] + '_' + settings2['overlap'] + '_' + settings2['region_size'] + '_' 
            
        original_parameters2 = '_' + settings1['scale'] + '_' + settings1['plateau'] + '_' 
        total_parameters2 = '_' + settings1['scale'] + '_' + settings1['plateau'] + '_' + settings2['plateau'] + '_' 

        total_file = file1.replace(original_parameters,total_parameters)
        total_file = total_file.replace(original_parameters2,total_parameters2)

        print total_file
    else:
        total_file = file1  

    community_list = load_intervals(total_file)

    buffer_genome = args.buffer*args.resolution
    buffer_genome_2 = buffer_genome#((args.buffer/2)+1)*args.resolution

    refined_calls = refine_final_calls(community_list,buffer_genome_2)            
    

    unique_boundaries,statistics = boundary_assessment(refined_calls,buffer_genome,args)
    final_calls(unique_boundaries,total_file,args)
    final_boundaries(unique_boundaries,total_file,args)
    print_statistics(statistics,total_file,args)


if __name__ == '__main__':
    main()

