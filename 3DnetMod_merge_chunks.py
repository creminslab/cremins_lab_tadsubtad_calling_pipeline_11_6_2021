from read_settings import read_settings
from load_community_dict import load_community_dict
from load_region_start_end_coord import load_region_start_end_coord
from create_tags import create_tags
from hierarchical_spatial_variance_minimization import HSVM
import sys 
import glob
import os
from remove_chaos_communities import remove_chaos_communities
from write_consistent_communities import write_consistent_communities
import numpy as np

def load_intervals_merging_chunks(results_path):
    input = open(results_path, 'r')
    communities = {}  

    for line in input:
        if line.startswith('#'):
            elements = line.strip().split('\t')
            test = len(elements)
        else:
            chromosome = str(line.strip().split('\t')[0])
            start1 = int(line.strip().split('\t')[1])
            start2 = int(line.strip().split('\t')[2])
            end1 = int(line.strip().split('\t')[3])
            end2 = int(line.strip().split('\t')[4])
            left_var = float(line.strip().split('\t')[5])
            right_var = float(line.strip().split('\t')[6])
            if chromosome not in communities:
                communities[chromosome] = []
            communities[chromosome].append([chromosome,start1,start2,end1,end2,left_var,right_var])
    input.close()

    return communities


def merge_chunks():

    settings1 = read_settings(sys.argv[1])
    tags1 = create_tags(sys.argv[1])
    tags_HSVM1 = tags1[3]

    settings2 = read_settings(sys.argv[2])
    tags2 = create_tags(sys.argv[2])
    tags_HSVM2 = tags2[3]

    if str(sys.argv[3]) != 'None':
        settings3 = read_settings(sys.argv[3])
        tags3 = create_tags(sys.argv[3])
        tags_HSVM3 = tags3[3]

    dir = 'output/HSVM/variance_thresholded_communities/merged/'
    final_output_one = 'Merged_' + 'Communities_*' + settings1['sample_1'] + '_' + tags_HSVM1 + '.txt'
    final_output_two = 'Merged_' + 'Communities_*' + settings2['sample_1'] + '_' + tags_HSVM2 + '.txt'
    if str(sys.argv[3]) != 'None':
        final_output_three = 'Merged_' + 'Communities_*' + settings3['sample_1'] + '_' + tags_HSVM3 + '.txt'

    #print "final_output_three: ",final_output_three

    file1 = glob.glob(dir + final_output_one)[0]
    file2 = glob.glob(dir + final_output_two)[0]
    if str(sys.argv[3]) != 'None':
        file3 = glob.glob(dir + final_output_three)[0] 
   
    community_list1 = load_intervals_merging_chunks(file1)
    community_list2 = load_intervals_merging_chunks(file2) 
    if str(sys.argv[3]) != 'None':
        community_list3 = load_intervals_merging_chunks(file3)


    original_parameters = settings1['overlap'] + '_' + settings1['region_size']
    if str(sys.argv[3]) != 'None':
        if (settings1['size_threshold'] != settings2['size_threshold']) or (settings1['size_threshold'] != settings3['size_threshold']):
            raise ValueError('different minimum size filters for different settings!')
        if (settings1['boundary_buffer'] != settings2['boundary_buffer']) or (settings1['boundary_buffer'] != settings3['boundary_buffer']):
            raise ValueError('different merging buffer for different settings!')
        total_parameters = settings1['overlap'] + '_' + settings1['region_size'] + '_' + settings2['overlap'] + '_' + settings2['region_size'] + \
        '_' + settings3['overlap'] + '_' + settings3['region_size']
    else:
        if (settings1['size_threshold'] != settings2['size_threshold']):
            raise ValueError('different minimum size filters for different settings!')
        if (settings1['boundary_buffer'] != settings2['boundary_buffer']):
             raise ValueError('different merging buffer for different settings!')
        total_parameters = settings1['overlap'] + '_' + settings1['region_size'] + '_' + settings2['overlap'] + '_' + settings2['region_size']

    output_file = file1.replace(original_parameters,total_parameters)

    original_parameters = settings1['scale'] + '_' + settings1['plateau'] + '_'
    if str(sys.argv[3]) != 'None':
        total_parameters = settings1['scale'] + '_' + settings1['plateau'] + '_' + settings2['plateau'] + '_' + settings3['plateau'] + '_'
    else:
        total_parameters = settings1['scale'] + '_' + settings1['plateau'] + '_' + settings2['plateau'] + '_'


    total_calls = {}

    for chrom in community_list1:
        #if chrom not in total_calls:
        #    total_calls[chrom] = []
        if str(sys.argv[3]) != 'None':
            total_calls[chrom] = community_list1[chrom] + community_list2[chrom] + community_list3[chrom] 
        else:
            total_calls[chrom] = community_list1[chrom] + community_list2[chrom] 
        total_calls[chrom].sort()  

    output_file = output_file.replace(original_parameters,total_parameters)

    output = open(output_file, 'w')    

    temp = '%s\t%s\t%s\t%s\t%s'%('#chr','start','end','left_var','right_var') 
    print>>output,temp

    for chrom in total_calls:
        for i in range(len(total_calls[chrom])):
            start1 = total_calls[chrom][i][1]
            start2 = total_calls[chrom][i][2]
            end1 = total_calls[chrom][i][3]
            end2 = total_calls[chrom][i][4]
            left_var = total_calls[chrom][i][5]
            right_var = total_calls[chrom][i][6]
            temp = '%s\t%d\t%d\t%d\t%d\t%f\t%f'%(total_calls[chrom][i][0],start1,start2,end1,end2,left_var,right_var)
            print >>output,temp

    output.close()




if __name__ == "__main__":
    merge_chunks()


            


    
