import sys
import glob
import os
import copy
import numpy as np
from merge_communities import merge_communities
from get_variance_distribution_and_threshold import get_variance_distribution_and_threshold
from plot_variance_distribution import *
from plot_size_distributions_per_stratum import plot_size_distributions_per_stratum
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import cm
from load_region_start_end_coord import load_region_start_end_coord
from create_bin_location_dict import create_bin_location_dict
import math
from scipy.stats import percentileofscore




def HSVM(settings,community_dict,sample, tag_HSVM, tag_preprocess, chr):
	"""
	This module takes an input dictionary of communities (key is region, value is list of community dictionaries) and identifies communities that do not have either boundary at the edge of the region (network). The communities are then thresholded based on the variance specified in the settings file. The resulting communities that pass the size and variance threshold are written to a results file. 
	"""
	# cell_type needs to have major chromosome in the name
	bad_communities = []
	sizes = []
	variance_settings = []
	variance_order = []
	final_communities = {}
	final_communities_sizes = {} # for troubleshooting
	variance_distributions = {}
	
	if 'trash_edge' not in settings:
		settings['trash_edge'] = int(25*(8000/float(settings['resolution'])))


	for size_inst in settings:
		if size_inst[:6]=='size_s':
			sizes.append(int(settings[size_inst]))
			final_communities[int(settings[size_inst])] = []
			final_communities_sizes[int(settings[size_inst])] = []
	if len(sizes) == 0:  #default place everything in single size
		sizes.append(int(1000000000))
		final_communities[int(1000000000)] = []
		final_communities_sizes[int(1000000000)] = []
	sizes = sorted(sizes)

	variance_tag = ''
	size_tag = ''
	for var_inst in settings:
		if var_inst[:10] == 'var_thresh':
			variance_settings.append(float(settings[var_inst]))
			variance_order.append(var_inst)
	if len(variance_settings) == 0: #default accept all domains - variance 100 AUC
		variance_settings.append(float(100)) 
		variance_order.append(100)  

	variance_settings = [x for (y,x) in sorted(zip(variance_order,variance_settings))]


	for item in variance_settings:
		variance_tag = variance_tag + str(item) + '_'


	for item in sizes:
		size = str(int(item)/1000) + 'kb'
		size_tag = size_tag + size + '_'

	if 'variance_type' not in settings:
		settings['variance_type'] = 'percent' 	

	if len(sizes) != len(variance_settings):
		print 'must have the same number of size stratifications as variance thresholds! Exiting!'
		return None

	for subchrom in community_dict:
		bed_file = 'input/' + subchrom + '_' + sample + '_' + tag_preprocess + "final.bed"
		print 'looking for bed_file: ', bed_file
		(region_idx, bin_size)  = load_region_start_end_coord(bed_file)
		bin_location = create_bin_location_dict(bed_file)
		for region in community_dict[subchrom]:
			start_boundary = region_idx[region]['start']
			end_boundary = region_idx[region]['end']
			for community in community_dict[subchrom][region]:
				if int(community['start']) - int(start_boundary)<=bin_size*int(settings['trash_edge']):
					bad_communities.append(community)
				elif int(end_boundary) - int(community['end']) <=bin_size*int(settings['trash_edge']):
					bad_communities.append(community)
				elif abs(int(community['end']) - int(community['start'])) <= bin_size*int(settings['size_threshold']):
					bad_communities.append(community)
				else:
					previous_size = 0
					for i in range(len(sizes)):
						if abs(int(community['end']) - int(community['start'])) > previous_size and abs(int(community['end']) - int(community['start'])) <= sizes[i]:
							final_communities[sizes[i]].append(community)
							final_communities_sizes[sizes[i]].append(abs(int(community['end']) - int(community['start'])))
						previous_size = sizes[i]


	#if settings['plots'] == 'True':
	#	plot_size_distributions_per_stratum(final_communities_sizes, chr + sample, sizes, tag_HSVM, settings)
	variance_distributions, var_thresholds, thresholded_communities = get_variance_distribution_and_threshold(final_communities, sizes, variance_settings,settings['variance_type'])


	total_thresholded_communities = []
	for i in range(len(sizes)):
		size = sizes[i]
		total_thresholded_communities += thresholded_communities[size]


	total_merged_communities = merge_communities(total_thresholded_communities, bin_size, int(settings['boundary_buffer']))
	community_thresholded_merged_return = {}
	
	dir = 'output/HSVM/variance_thresholded_communities/merged/'
	if not os.path.isdir(dir): os.makedirs(dir)
	#final_output = 'Merged_' + 'Communities_' + chr + sample  + '_' + tag_HSVM + '.txt'
	#community_output_thresholded_merged = open(dir + final_output, 'w')
	#header = '#Chr\tUpstream_boundary_start\tUpstream_boundary_stop\tDownstream_boundary_start\tDownstream_boundary_stop'
	#print >> community_output_thresholded_merged, header
	for community in total_merged_communities:
		#temp = community['chr'] + "\t" + str(community['start1']) + "\t" + str(community['start2']) + "\t" + str(community['stop1']) + "\t" + str(community['stop2']) + "\t" + str(community['left_var']) + "\t" + str(community['right_var'])
		if community['chr'] not in community_thresholded_merged_return:
			community_thresholded_merged_return[community['chr']] = []
		community_thresholded_merged_return[community['chr']].append([community['chr'],community['start1'],community['start2'],community['stop1'],community['stop2'],\
			community['left_var'],community['right_var']])
		#print >> community_output_thresholded_merged, temp
	#community_output_thresholded_merged.close()
	
	return community_thresholded_merged_return

