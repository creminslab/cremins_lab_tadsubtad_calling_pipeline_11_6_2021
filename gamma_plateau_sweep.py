import glob
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as pyplot
from counts_to_array import counts_to_array
from construct_nulls import construct_NG_null
from make_scatterplots import make_scatterplots
from make_scatterplots import make_scatterplots_zoom
from real_Qs_avg import real_Qs_avg
from get_plateaus import get_plateaus
from save_avg_num_comm_v_gamma import save_avg_num_comm_v_gamma
from save_gammas_in_range_abbreviated import save_gammas_in_range
from convert_tag_genomewide import convert_tag_genomewide

"""
Heidi Norton, Daniel Emerson

Determine the useful range of the Structural Resolution parameter, gamma, by computing modularity for a real and randomly rewired networks across a range of structural resolution parameter values.

Final useful gamma is determined as the gamma at which the modularity of the real network converges with modularity of the random network.

Assumes convergence has to occur after 1.5 gamma.

User defined settings should be specified in settings.txt

"""

def GPS(chr, settings, sample, badregions,tag_GPS,tag_MMCP,tag_preprocess, max_gamma):

	if 'single_gamma' in settings.keys():
		return 0

	tag_GPS_test = convert_tag_genomewide(tag_GPS, settings)
	tag_MMCP_test = convert_tag_genomewide(tag_MMCP, settings)


	if 'num_rand' not in settings:
		settings['num_rand'] = 1

	num_rand = int(settings['num_rand'])
	cell_type_input = str(chr) + '_' + sample
	counts = 'input/' +  cell_type_input + '_' + tag_preprocess + "finalpvalues.counts"
	bed = 'input/' +  cell_type_input + '_' + tag_preprocess + "final.bed"
	cell_type = str(chr) + sample
	counts_as_arrays,bin_location = counts_to_array(counts, bed)
	if eval(settings['badregionfilter']):
		print "badregions: ",badregions
		for instance in badregions:
			for region in instance:
				if instance[region] == chr:
					if region in counts_as_arrays:
						del counts_as_arrays[region]
	gammas_finer = {}
	num_comm = {}
	real_Q_finer = {}
	random_Qs_finer = {}
	plateau_dict = {}

	if 'gamma_step' not in settings:
		settings['gamma_step'] = 0.01 #set default to 0.01 if missing
	if 'plots' not in settings:
		settings['plots'] = 'False'

	gamma_step = float(settings['gamma_step'])

	try:
		x = 1/int(1/gamma_step)
	except ZeroDivisionError:
		print("Cannot exceed step size of 1 gamma")

	if max_gamma == None:
		return None
	else:

		for region in counts_as_arrays:
			real_Q = 1
			gamma = 0.0
			gammas_finer[region] = []
			num_comm[region] = []
			real_Q_finer[region] = []
			random_Qs_finer[region] = []

			# Perform a finer sampling of gamma to get plateaus in avg # community v gamma
			gamma = 0
			mod = []
			A = counts_as_arrays[region]
			np.fill_diagonal(A, 0) # zero out the diagonal of the network to match random rewiring scenario
			NG = construct_NG_null(A) # construct the Newman  Girvan null model
			while gamma <= max_gamma:
				real_Q,num_communities  = real_Qs_avg(settings,A,NG,gamma)
				gammas_finer[region].append(gamma)
				num_comm[region].append(num_communities)
				real_Q_finer[region].append(real_Q)
				gamma = gamma + float(settings['gamma_step'])


			identifier = cell_type + '_' + chr + region + '_' + tag_GPS
			# Make plots
			if settings['plots'] == 'True':

				filename = 'Avg_num_comm_v_gamma_' + identifier + '.png'
				make_scatterplots(gammas_finer[region], num_comm[region], filename, 'output/GPS/scatterplots/')

				filename = 'Avg_num_comm_v_gamma_zoom1' + identifier + '.png'
				make_scatterplots_zoom(gammas_finer[region], num_comm[region], filename, 'output/GPS/scatterplots/', (2,3))


				filename = 'Avg_num_comm_v_gamma_zoom2' + identifier + '.png'
				make_scatterplots_zoom(gammas_finer[region], num_comm[region], filename, 'output/GPS/scatterplots/', (3,4))

				filename = 'Avg_num_comm_v_Q_' + identifier + '.png'
				make_scatterplots(real_Q_finer[region], num_comm[region], filename, 'output/GPS/scatterplots/')

				filename = 'Q_v_Avg_num_comm_' + identifier + '.png'
				make_scatterplots(num_comm[region], real_Q_finer[region], filename, 'output/GPS/scatterplots/')

				filename = 'Q_v_gamma_' + identifier + '.png'
				make_scatterplots(gammas_finer[region], real_Q_finer[region], filename, 'output/GPS/scatterplots/')


		plateau_gammas = {}
		for region in counts_as_arrays:
			plateau_gammas[region] = []
			plateau = get_plateaus(num_comm[region], gammas_finer[region], int(settings['plateau']))
			for avg_num_comm in plateau:
				if avg_num_comm > 1:   # only want to keep gammas that lead to at least 2 communities
					plateau_gammas[region].append(np.mean(plateau[avg_num_comm]))

			print 'final gammas: '
			print plateau_gammas[region]
			plateau_gammas[region].sort() 
		gammas = {}
		for region in plateau_gammas:
			gammas[region] = []
			for i in range(len(plateau_gammas[region])):
				gammas[region].append(plateau_gammas[region][i])
		return gammas




if __name__ == '__main__':
	main()
