def create_tags(filename):
	"""
	General settings file reader used to provide other scripts with user-edited inputs from a text file

	INPUTS
	------
	filename, str
		the filename and location of the settings file. The file should have the format of a category tab separated from string value

	OUTPUT
	------
	var_dict, dict
		a dictionary constructed from the settings file where key = category and value = the stored values
	"""

	preprocess_tag = ''
	gps_tag = ''

	found_uniform_evaluation = False
	found_badregion_file = False

	#preprocess tag creation
	with open(filename, 'r') as input:
		for line in input:
			if line.startswith('#GPS#'):
				break # ignore empty lines and comments
			if line.startswith('bed_file'):
				continue
			if line.startswith('sample'):
				continue
			if line.startswith('counts'):
				continue
			if line.startswith('processors'):
				continue
			if line.startswith('resolution'):
				continue
			pair = line.strip().split('\t')
			if len(pair) == 1: #test to see if user put space separatation
				a = line.strip().split(' ')
				pair = list(filter(lambda x: x!= '', a))
			if len(pair) > 1:
				if pair == ['uniform_size','False']:
					preprocess_tag  = preprocess_tag +  'F_' 
					found_uniform_evaluation = True
				elif pair == ['uniform_size','True']:
					preprocess_tag  = preprocess_tag +  'T_'
					found_uniform_evaluation = True
				elif pair[0] == 'logged':
					preprocess_tag  = preprocess_tag + "L"+ pair[1] + '_'
					logged_evaluation = pair[1]
				else:
					preprocess_tag  = preprocess_tag + pair[1] + '_'
	input.close()

	if not found_uniform_evaluation:
		preprocess_tag = preprocess_tag.replace("_L"+ logged_evaluation,"_F_"+ logged_evaluation)
	else:
		preprocess_tag = preprocess_tag.replace("L"+ logged_evaluation,logged_evaluation)
 
	preprocess_tag = preprocess_tag[:-1]
	nextup = True
	gps_tag = preprocess_tag

	#gps tag creation
	with open(filename, 'r') as input:
		for line in input:
			if line.startswith('#GPS#'):
				nextup = False
				continue
			if line.startswith('badregionfile'):
				found_badregion_file = True
			if line.startswith('switch'):
				continue
			if line.startswith('seed_file'):
				continue
			if line.startswith('hierarchies'):
				continue
			if line.startswith('plots'):
				continue
			if nextup:
				continue
			if line.startswith('#MMCP#'):
				break
			if line.startswith('#HSVM#'):
				break
			pair = line.strip().split('\t')
			if len(pair) == 1: #test to see if user put space separatation
				a = line.strip().split(' ')
				pair = list(filter(lambda x: x!= '', a))
			if len(pair) > 1:
				if pair[0] == 'badregionfilter':
					gps_tag  = gps_tag + '_bfilter' +  pair[1]
					badregionfilter_evaluation = pair[1]
				else:
					gps_tag  = gps_tag + '_' +  pair[1]
	input.close()


	if not found_badregion_file:
		gps_tag = gps_tag.replace("_bfilter"+ badregionfilter_evaluation,"_None_"+ badregionfilter_evaluation)
	else:
		gps_tag = gps_tag.replace("bfilter"+ badregionfilter_evaluation,badregionfilter_evaluation)

	nextup = True
	mmcp_tag = gps_tag

	#mmcp tag creation
	with open(filename, 'r') as input:
		for line in input:
			if line.startswith('#HSVM#'):
				break
			if line.startswith('#MMCP#'):
				nextup = False
				continue
			if line.startswith('switch'):
				continue
			if line.startswith('seed_file'):
				continue
			if line.startswith('hierarchies'):
				continue
			if line.startswith('plots'):
				continue
			if nextup:
				continue
			pair = line.strip().split('\t')
			if len(pair) == 1: #test to see if user put space separatation
				a = line.strip().split(' ')
				pair = list(filter(lambda x: x!= '', a))
			if len(pair) > 1:
				mmcp_tag  = mmcp_tag + '_' +  pair[1]
				print "adding tag"
	input.close()


	print "mmcp_tag: ",mmcp_tag

	nextup = True
	hsvm_tag = mmcp_tag
	size_list = []
	var_list = []
	
	#hsvm tag creation
	with open(filename, 'r') as input:
		for line in input:
			if line.startswith('#HSVM#'):
				nextup = False
				continue
			if nextup:
				continue
			if line.startswith('#DBR#'):
				break
			if ("" == line):
				break
			if line.startswith('size_s'):
				line = line[5:]
				pair = line.strip().split('\t')
				if len(pair) == 1: #test to see if user put space separatation
					a = line.strip().split(' ')
					pair = list(filter(lambda x: x!= '', a))
				if len(pair) > 1:
					hsvm_tag = hsvm_tag + '_' + pair[0] + '_' + str(int(pair[1])/1000) + 'kb'
			elif line.startswith('var_t'):
				line = line[4:]
				pair = line.strip().split('\t')
				if len(pair) == 1: #test to see if user put space separatation
					a = line.strip().split(' ')
					pair = list(filter(lambda x: x!= '', a))
				if len(pair) > 1:
					hsvm_tag = hsvm_tag + '_v' + pair[0][-1] + '_' + pair[1]
			else:
				pair = line.strip().split('\t')
				if len(pair) == 1: #test to see if user put space separatation
					a = line.strip().split(' ')
					pair = list(filter(lambda x: x!= '', a))
				if len(pair) > 1:
					hsvm_tag  = hsvm_tag + '_' +  pair[1]  
	input.close()

	nextup = True
	dbr_tag = hsvm_tag

	#dbr tag creation
	with open(filename, 'r') as input:
		for line in input:
			if line.startswith('#DBR#'):
				nextup = False
				continue 
			if nextup:
				continue
			pair = line.strip().split('\t')
			if len(pair) == 1: #test to see if user put space separatation
				a = line.strip().split(' ')
				pair = list(filter(lambda x: x!= '', a))
			if len(pair) > 1:
				dbr_tag  = dbr_tag + '_' +  pair[1]  
	input.close()

	return preprocess_tag,gps_tag,mmcp_tag,hsvm_tag,dbr_tag
