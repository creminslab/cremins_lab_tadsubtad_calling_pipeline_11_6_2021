"""
05/23/16
Heidi Norton and Jennifer Phillips-Cremins

Writes a community results file of the format:
#Region\tGamma_Parameter\tCommunity_Assignment\tChr\tStart_Pos\tStop_Pos


Input:
method: method used to determine significant consensus partitions

Modified 6/12/16 to only write communities that have a number > 0

"""
import os
import numpy as np
from flatten_counts_single_region_geometric import flatten_counts_single_region

def create_results_output_genomewide_remove_trash(communities_dict, level_variances, bin_location, corrected_counts_file_input, settings, counts_as_array, hybrid,tag):
	"""
	Example communities_dict[region][gamma]: [1 1 1 2 2 2 3 3 4 4 5 5]
	Example boundaries, aka level_variances[region][gamma][0]: [0, 3, 6, 8, 10, 12]
	Note that the boundaries range from 0 to 12 although there are only 12 nodes in the sample network. 
	
	
	"""
	
	if 'pctile_threshold' not in settings:
		settings['pctile_threshold'] = 0 #default 0 (accept all)
	if 'pct_value' not in settings:
		settings['pct_value'] = 0 #default 0 (accept all)

	community_thresholded = {}
	for region in communities_dict:
		counts = counts_as_array[region]
		np.fill_diagonal(counts, np.max(counts))
		threshold = np.percentile(flatten_counts_single_region(counts, discard_nan=True),float(settings['pctile_threshold']))
		for gamma in communities_dict[region]: 
			for i in range(level_variances[region][gamma].shape[1]-1):
				community_start = int(level_variances[region][gamma][0,i])
				community_stop = int(level_variances[region][gamma][0, i+1] - 1) # community_start to community_stop includes the bin numbers contained within the community
				comm_column = counts[int(community_start):int(community_stop+1), int(community_stop)] #
				pct_value = float(len(comm_column[comm_column>threshold]))/float(len(comm_column)) # pct value is the % of counts along the edge of the community that must be greater than a given threshold, defined by a pctile count within the region
				community_number = int(communities_dict[region][gamma][community_start])
				community_start_coord = int(bin_location[region][community_start][2])
				community_stop_coord = int(bin_location[region][community_stop][3])
				left_var = float(level_variances[region][gamma][1,i])
				right_var = float(level_variances[region][gamma][1, i+1])
				chr= bin_location[region][i][1]

				community_coordinates = ((chr,int(community_start_coord),int(community_stop_coord)))
				community = {'region':region,'gamma':gamma,'number':community_number,'chr':chr,'start':community_start_coord,'stop':community_stop_coord,'left_var':left_var,'right_var':right_var,'hierarchy':gamma}
				if community_stop - community_start + 1 >=2: # ensure that all communities are at least 2 nodes
					if pct_value >= float(settings['pct_value']): # line to modify for pct 
						if community_coordinates not in community_thresholded:
							community_thresholded[community_coordinates] = {}
							community_thresholded[community_coordinates]['community_list'] = []
							community_thresholded[community_coordinates]['variance_list'] = []
						community_thresholded[community_coordinates]['community_list'].append(community)
						community_thresholded[community_coordinates]['variance_list'].append(left_var + right_var)

	return community_thresholded
