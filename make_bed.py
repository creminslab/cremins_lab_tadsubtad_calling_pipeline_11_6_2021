import numpy as np
import scipy.sparse
import argparse
import subprocess
import os

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('qnorm_chr1', type=str, help="chr1 qnorm counts")
    parser.add_argument('qnorm_chr2', type=str, help="chr2 qnorm counts")
    parser.add_argument('qnorm_chr3', type=str, help="chr3 qnorm counts")
    parser.add_argument('qnorm_chr4', type=str, help="chr4 qnorm counts")
    parser.add_argument('qnorm_chr5', type=str, help="chr5 qnorm counts")
    parser.add_argument('qnorm_chr6', type=str, help="chr6 qnorm counts")
    parser.add_argument('qnorm_chr7', type=str, help="chr7 qnorm counts")
    parser.add_argument('qnorm_chr8', type=str, help="chr8 qnorm counts")
    parser.add_argument('qnorm_chr9', type=str, help="chr9 qnorm counts")
    parser.add_argument('qnorm_chr10', type=str, help="chr10 qnorm counts")
    parser.add_argument('qnorm_chr11', type=str, help="chr11 qnorm counts")
    parser.add_argument('qnorm_chr12', type=str, help="chr12 qnorm counts")    
    parser.add_argument('qnorm_chr13', type=str, help="chr13 qnorm counts")
    parser.add_argument('qnorm_chr14', type=str, help="chr14 qnorm counts")    
    parser.add_argument('qnorm_chr15', type=str, help="chr15 qnorm counts")
    parser.add_argument('qnorm_chr16', type=str, help="chr16 qnorm counts")
    parser.add_argument('qnorm_chr17', type=str, help="chr17 qnorm counts")
    parser.add_argument('qnorm_chr18', type=str, help="chr18 qnorm counts")
    parser.add_argument('qnorm_chr19', type=str, help="chr19 qnorm counts")
    parser.add_argument('qnorm_chr20', type=str, help="chr20 qnorm counts")
    parser.add_argument('qnorm_chr21', type=str, help="chr21 qnorm counts")
    parser.add_argument('qnorm_chr22', type=str, help="chr22 qnorm counts")
    parser.add_argument('qnorm_chrX', type=str, help="chrX qnorm counts")
    parser.add_argument('resolution', type=int, help="resolution")
    parser.add_argument('dir', type=str, help="directory")
    parser.add_argument('file', type=str, help="bed_name")  

    args = parser.parse_args()

    qnorm_chr1 = scipy.sparse.load_npz(args.qnorm_chr1).tocoo()
    qnorm_chr2 = scipy.sparse.load_npz(args.qnorm_chr2).tocoo()
    qnorm_chr3 = scipy.sparse.load_npz(args.qnorm_chr3).tocoo()
    qnorm_chr4 = scipy.sparse.load_npz(args.qnorm_chr4).tocoo()
    qnorm_chr5 = scipy.sparse.load_npz(args.qnorm_chr5).tocoo()
    qnorm_chr6 = scipy.sparse.load_npz(args.qnorm_chr6).tocoo()
    qnorm_chr7 = scipy.sparse.load_npz(args.qnorm_chr7).tocoo()
    qnorm_chr8 = scipy.sparse.load_npz(args.qnorm_chr8).tocoo()
    qnorm_chr9 = scipy.sparse.load_npz(args.qnorm_chr9).tocoo()
    qnorm_chr10 = scipy.sparse.load_npz(args.qnorm_chr10).tocoo()
    qnorm_chr11 = scipy.sparse.load_npz(args.qnorm_chr11).tocoo()
    qnorm_chr12 = scipy.sparse.load_npz(args.qnorm_chr12).tocoo()
    qnorm_chr13 = scipy.sparse.load_npz(args.qnorm_chr13).tocoo()
    qnorm_chr14 = scipy.sparse.load_npz(args.qnorm_chr14).tocoo()
    qnorm_chr15 = scipy.sparse.load_npz(args.qnorm_chr15).tocoo()
    qnorm_chr16 = scipy.sparse.load_npz(args.qnorm_chr16).tocoo()
    qnorm_chr17 = scipy.sparse.load_npz(args.qnorm_chr17).tocoo()
    qnorm_chr18 = scipy.sparse.load_npz(args.qnorm_chr18).tocoo()
    qnorm_chr19 = scipy.sparse.load_npz(args.qnorm_chr19).tocoo()
    qnorm_chr20 = scipy.sparse.load_npz(args.qnorm_chr20).tocoo()
    qnorm_chr21 = scipy.sparse.load_npz(args.qnorm_chr21).tocoo()
    qnorm_chr22 = scipy.sparse.load_npz(args.qnorm_chr22).tocoo()
    qnorm_chrX = scipy.sparse.load_npz(args.qnorm_chrX).tocoo()


    start_bin = 0
    
    chromosomes = [qnorm_chr1,qnorm_chr2,qnorm_chr3,qnorm_chr4,qnorm_chr5,qnorm_chr6,qnorm_chr7,qnorm_chr8,qnorm_chr9,qnorm_chr10,qnorm_chr11,qnorm_chr12,qnorm_chr13,qnorm_chr14,qnorm_chr15,qnorm_chr16,qnorm_chr17,qnorm_chr18,qnorm_chr19,qnorm_chr20,qnorm_chr21,qnorm_chr22,qnorm_chrX]
    chr_labels = ['chr1','chr2','chr3','chr4','chr5','chr6','chr7','chr8','chr9','chr10','chr11','chr12','chr13','chr14','chr15','chr16','chr17','chr18','chr19','chr20','chr21','chr22','chrX'] 

    output_file = args.dir + args.file

    output = open(output_file, 'w')

    for i in range(0,len(chromosomes)):
        start_coord_chr = 0
        for j in range(0,chromosomes[i].shape[0]):
            temp =  '%s\t%d\t%d\t%d'%(chr_labels[i],args.resolution*start_coord_chr,args.resolution*(start_coord_chr + 1),start_bin + 1)
            start_bin = start_bin + 1
            start_coord_chr = start_coord_chr + 1
            print >> output,temp

    #command_sort = "sort -k1,1 -k2,2n ' " + output_file + " > " + output_file[:-4] + "_sort.bed" 
    #subprocess.call(command_sort,shell=True) 

    #command_remove = "rm " + output_file
    #subprocess.call(command_remove,shell=True)    


if __name__ == '__main__':
    main()
